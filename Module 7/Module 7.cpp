﻿#include <iostream>

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;

public:
    Vector() : x(5), y(5), z(5)
    {}
    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << x << ' ' << y << ' ' << z << '\n';
    }

    void Len()
    {
        std::cout  << sqrt(x*x+y*y+z*z);
    }

};

int main()
{
    Vector v;
    v.Show();
    v.Len();
   
}


